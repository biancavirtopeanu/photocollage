var templateid;
window.onload = function () {
    //am initializat toate elementele dupa incarcarea paginii
    var imageLoader = document.getElementById('filePhoto');
    var imageLoader2 = document.getElementById('filePhoto2');
    var imageLoader3 = document.getElementById('filePhoto3');
    imageLoader.addEventListener('change', handleImage, false);
    imageLoader2.addEventListener('change', handleImage, false);
    imageLoader3.addEventListener('change', handleImage, false);
    //primul loc pentru upload
    uploader1 = document.getElementById("uploader");
    uploader1.addEventListener("dragenter", dragenter, false);
    uploader1.addEventListener("dragover", dragover, false);
    uploader1.addEventListener("drop", drop, false);
    //al doilea loc pentru upload
    uploader2 = document.getElementById("uploader2");
    uploader2.addEventListener("dragenter", dragenter, false);
    uploader2.addEventListener("dragover", dragover, false);
    uploader2.addEventListener("drop", drop, false);
    //al treilea loc pentru upload
    uploader3 = document.getElementById("uploader3");
    uploader3.addEventListener("dragenter", dragenter, false);
    uploader3.addEventListener("dragover", dragover, false);
    uploader3.addEventListener("drop", drop, false);
    //butonul de download
    document.getElementById('downloadBtn').addEventListener('click', function () {
        saveCanvas(this, 'canvas', 'myCollage.png');
    }, false);

    //am pus event de click pe efecte
    document.getElementById('gray1').addEventListener('click', applyFilter, false);
    document.getElementById('brightness1').addEventListener('click', applyFilter, false);
    document.getElementById('gray2').addEventListener('click', applyFilter, false);
    document.getElementById('brightness2').addEventListener('click', applyFilter, false);
    document.getElementById('gray3').addEventListener('click', applyFilter, false);
    document.getElementById('brightness3').addEventListener('click', applyFilter, false);
    document.getElementById('removeEffects').addEventListener('click', applyFilter, false);
}

function handleImage(e) {
    //verific pe ce uploader trebuie sa pun imaginea
    if(e.currentTarget.id === 'filePhoto'){
        var reader = new FileReader();
        //pun imaginea pe elementul din div-ul cu id-ul uploader care e img
        reader.onload = function (event) {
            $('#uploader img').attr('src', event.target.result);
        }
        reader.readAsDataURL(e.target.files[0]);
    } else if (e.currentTarget.id === 'filePhoto2') {
        var reader = new FileReader();
        reader.onload = function (event) {
            $('#uploader2 img').attr('src', event.target.result);
        }
        reader.readAsDataURL(e.target.files[0]);
    } if (e.currentTarget.id === 'filePhoto3') {
        var reader = new FileReader();
        reader.onload = function (event) {
            $('#uploader3 img').attr('src', event.target.result);
        }
        reader.readAsDataURL(e.target.files[0]);
    }
}

function dragenter(e) {
    //anuleaza actiunea default a eventului
    e.stopPropagation();
    //actiunea implicita a evenimentului nu va fi declansata
    e.preventDefault();
}

function dragover(e) {
    e.stopPropagation();
    e.preventDefault();
}

function drop(e) {
    var imageLoader = document.getElementById('filePhoto');
    var imageLoader2 = document.getElementById('filePhoto2');
    var imageLoader3 = document.getElementById('filePhoto3'); 
    imageLoader.addEventListener('change', handleImage, false);
    imageLoader2.addEventListener('change', handleImage, false);
    imageLoader3.addEventListener('change', handleImage, false);
    e.stopPropagation();
    e.preventDefault();
    //console.log(e);
    //aici gasim datele care se gasesc la operatia de draganddrop
    var data = e.dataTransfer;
    var files = data.files;
    //aici se declanseaza handleImage (imageLoader change event)
    if (e.currentTarget.id === 'uploader') {
        imageLoader.files = files;
    } else if (e.currentTarget.id ==='uploader2') {
        imageLoader2.files = files;
    }
    else if (e.currentTarget.id === 'uploader3') {
        imageLoader3.files = files;
    }
    
}

function removeImages() {
    //se aude sunetul pentru stergerea fotografiilor
    var audioRemove = document.getElementById("removeAudio");
    audioRemove.play();
    //se pune o sursa goala pe imagini
    $('#uploader img').attr('src', '');
    $('#uploader2 img').attr('src', '');
    $('#uploader3 img').attr('src', '');
    //se preia canvasul
    var canvas = $("#canvas")[0];
    var ctx = canvas.getContext("2d");
    canvasDimensionWidth = canvas.width;
    canvasDimensionHeigth = canvas.height;
    //se curata canvasul pe intreaga lui lungime/latime
    ctx.clearRect(0, 0, canvasDimensionWidth, canvasDimensionHeigth);
}

//functie pentru desenarea liniilor/imaginilor(daca au fost uploadate) in canvas
function drawTemplate(id) {
    //am preluat imaginile
    var picture1 = document.getElementById('picture1');
    var picture2 = document.getElementById('picture2');
    var picture3 = document.getElementById('picture3');
    var canvas = $("#canvas")[0];
    var ctx = canvas.getContext("2d");
    canvasDimensionWidth = canvas.width;
    canvasDimensionHeigth = canvas.height;
    switch (id) {
        case 1: {
            templateid = 1;
            //daca exista imagini uploadate pt primele doua imagini se deseneaza in canvas conform modelului 1
            if (picture1.getAttribute('src') !== "" && picture2.getAttribute('src') !== "") {
                ctx.clearRect(0, 0, canvasDimensionWidth, canvasDimensionHeigth);
                ctx.drawImage(picture1, 0 , 0, canvasDimensionWidth, canvasDimensionHeigth / 2);
                ctx.drawImage(picture2, 0, canvasDimensionHeigth/2, canvasDimensionWidth, canvasDimensionHeigth/2);
                break;
                //daca exista imagini uploadate pt imaginea 1 si 3 se deseneaza in canvas conform modelului 1
            } else if (picture1.getAttribute('src') !== "" && picture3.getAttribute('src') !== "") {
                ctx.clearRect(0, 0, canvasDimensionWidth, canvasDimensionHeigth);
                ctx.drawImage(picture1, 0, 0, canvasDimensionWidth, canvasDimensionHeigth / 2);
                ctx.drawImage(picture3, 0, canvasDimensionHeigth / 2, canvasDimensionWidth, canvasDimensionHeigth / 2);
                break;
                //daca exista imagini uploadate pt imaginea 2 si 3 se deseneaza in canvas conform modelului 1
            } else if (picture2.getAttribute('src') !== "" && picture3.getAttribute('src') !== "") {
                ctx.clearRect(0, 0, canvasDimensionWidth, canvasDimensionHeigth);
                ctx.drawImage(picture2, 0, 0, canvasDimensionWidth, canvasDimensionHeigth / 2);
                ctx.drawImage(picture3, 0, canvasDimensionHeigth / 2, canvasDimensionWidth, canvasDimensionHeigth / 2);
                break;
            }
                //se deseneaza doar liniile
            else {
                alert("Please upload your pictures!");
                ctx.clearRect(0, 0, canvasDimensionWidth, canvasDimensionHeigth);
                ctx.beginPath();
                ctx.moveTo(0, canvasDimensionHeigth / 2);
                ctx.lineTo(canvasDimensionWidth, canvasDimensionHeigth / 2);
                ctx.stroke();
                ctx.closePath();
                break;
            }
        }
        case 2: {
            templateid = 2;
            if (picture1.getAttribute('src') !== "" && picture2.getAttribute('src') !== "") {
                ctx.clearRect(0, 0, canvasDimensionWidth, canvasDimensionHeigth);
                ctx.drawImage(picture1, 0, 0, canvasDimensionWidth / 2, canvasDimensionHeigth);
                ctx.drawImage(picture2, canvasDimensionWidth / 2, 0, canvasDimensionWidth / 2, canvasDimensionHeigth);
                break;

            } else if (picture1.getAttribute('src') !== "" && picture3.getAttribute('src') !== "") {
                ctx.clearRect(0, 0, canvasDimensionWidth, canvasDimensionHeigth);
                ctx.drawImage(picture1, 0, 0, canvasDimensionWidth / 2, canvasDimensionHeigth);
                ctx.drawImage(picture3, canvasDimensionWidth / 2, 0, canvasDimensionWidth / 2, canvasDimensionHeigth);
                break;

            } else if (picture2.getAttribute('src') !== "" && picture3.getAttribute('src') !== "") {
                ctx.clearRect(0, 0, canvasDimensionWidth, canvasDimensionHeigth);
                ctx.drawImage(picture2, 0, 0, canvasDimensionWidth / 2, canvasDimensionHeigth);
                ctx.drawImage(picture3, canvasDimensionWidth / 2, 0, canvasDimensionWidth / 2, canvasDimensionHeigth);
                break;
            }
            else {
                alert("Please upload your pictures!");
                ctx.clearRect(0, 0, canvasDimensionWidth, canvasDimensionHeigth);
                ctx.beginPath();
                ctx.moveTo(canvasDimensionWidth / 2, 0);
                ctx.lineTo(canvasDimensionWidth / 2, canvasDimensionHeigth);
                ctx.stroke();
                ctx.closePath();
                break;
            }
        }
        case 3: {
            templateid = 3;
            if (picture1.getAttribute('src') !== "" && picture3.getAttribute('src') !== "" && picture3.getAttribute('src') !== "") {
                ctx.clearRect(0, 0, canvasDimensionWidth, canvasDimensionHeigth);
                ctx.drawImage(picture1, 0, 0, canvasDimensionWidth * 1 / 3, canvasDimensionHeigth);
                ctx.drawImage(picture2, canvasDimensionWidth * 1 / 3, 0, canvasDimensionWidth * 1 / 3, canvasDimensionHeigth);
                ctx.drawImage(picture3, canvasDimensionWidth * 2 / 3, 0, canvasDimensionWidth * 1 / 3, canvasDimensionHeigth);   
            } else {
                alert("Please upload your pictures!");
                ctx.clearRect(0, 0, canvasDimensionWidth, canvasDimensionHeigth);
                ctx.beginPath();
                ctx.moveTo(canvasDimensionWidth / 3, 0);
                ctx.lineTo(canvasDimensionWidth / 3, canvasDimensionHeigth);
                ctx.moveTo(canvasDimensionWidth * 2 / 3, 0);
                ctx.lineTo(canvasDimensionWidth * 2 / 3, canvasDimensionHeigth);
                ctx.moveTo(canvasDimensionWidth * 1 / 3, 0);
                ctx.lineTo(canvasDimensionWidth * 1 / 3, canvasDimensionHeigth);
                ctx.stroke();
                ctx.closePath();
            }
          
            break;
        }
        case 4: {
            templateid = 4;
            if (picture1.getAttribute('src') !== "" && picture3.getAttribute('src') !== "" && picture3.getAttribute('src') !== "") {
                ctx.clearRect(0, 0, canvasDimensionWidth, canvasDimensionHeigth);
                ctx.drawImage(picture1, 0, 0, canvasDimensionWidth, canvasDimensionHeigth / 2);
                ctx.drawImage(picture2, 0, canvasDimensionHeigth / 2, canvasDimensionWidth / 2, canvasDimensionHeigth / 2);
                ctx.drawImage(picture3, canvasDimensionWidth / 2, canvasDimensionHeigth / 2, canvasDimensionWidth / 2, canvasDimensionHeigth / 2);
            } else {
                ctx.clearRect(0, 0, canvasDimensionWidth, canvasDimensionHeigth);
                alert("Please upload your pictures!");
                ctx.beginPath();
                ctx.moveTo(0, canvasDimensionHeigth / 2);
                ctx.lineTo(canvasDimensionWidth, canvasDimensionHeigth / 2);
                ctx.moveTo(canvasDimensionWidth / 2, canvasDimensionHeigth / 2);
                ctx.lineTo(canvasDimensionWidth / 2, canvasDimensionWidth);
                ctx.stroke();
                ctx.closePath();
            }
            break;
        }
    }
}

function saveCanvas(link, canvasId, filename) {
    //se aude sunetul pentru doawnload
    var audioDownload = document.getElementById("downloadAudio");
    audioDownload.play();
    //se salveaza cu denumirea "myCollage" pusa mai sus in window.onLoad
    link.href = document.getElementById(canvasId).toDataURL();
    link.download = filename;
}

function applyFilter(e) {
    switch (e.currentTarget.id) {
        //am folosit "brightnessX ca sa stiu exact coordonatele unde trebuie sa modific valoare pixelilor"
        case 'brightness1': {
            switch (templateid) {
                case 1: {
                    filterBrightness(0, 0, canvasDimensionWidth, canvasDimensionHeigth / 2);
                    break;
                }
                case 2: {
                    filterBrightness(0, 0, canvasDimensionWidth / 2, canvasDimensionHeigth);
                    break;
                }
                case 3: {
                    filterBrightness(0, 0, canvasDimensionWidth * 1 / 3, canvasDimensionHeigth);
                    break;
                }
                case 4: {
                    filterBrightness(0, 0, canvasDimensionWidth, canvasDimensionHeigth / 2);
                    break;
                }
            }
         
            break;
        }
        case 'brightness2': {
            switch (templateid) {
                case 1: {
                    filterBrightness(0, canvasDimensionHeigth / 2, canvasDimensionWidth, canvasDimensionHeigth / 2);
                    break;
                }
                case 2: {
                    filterBrightness(canvasDimensionWidth / 2, 0, canvasDimensionWidth / 2, canvasDimensionHeigth);
                    break;
                }
                case 3: {
                    filterBrightness(canvasDimensionWidth * 1 / 3, 0, canvasDimensionWidth * 1 / 3, canvasDimensionHeigth);
                    break;
                }
                case 4: {
                    filterBrightness(0, canvasDimensionHeigth / 2, canvasDimensionWidth / 2, canvasDimensionHeigth / 2);
                    break;
                }
            }

            break;
        }
        case 'brightness3': {
            switch (templateid) {
                case 1: {
                    filterBrightness(0, canvasDimensionHeigth / 2, canvasDimensionWidth, canvasDimensionHeigth / 2);
                    break;
                }
                case 2: {
                    filterBrightness(canvasDimensionWidth / 2, 0, canvasDimensionWidth / 2, canvasDimensionHeigth);
                    break;
                }
                case 3: {
                    filterBrightness(canvasDimensionWidth * 2 / 3, 0, canvasDimensionWidth * 1 / 3, canvasDimensionHeigth);
                    break;
                }
                case 4: {
                    filterBrightness(canvasDimensionWidth / 2, canvasDimensionHeigth / 2, canvasDimensionWidth / 2, canvasDimensionHeigth / 2);
                    break;
                }
            }
            break;
        }
        case 'gray1': {
            switch (templateid) {
                case 1: {
                    filterGray(0, 0, canvasDimensionWidth, canvasDimensionHeigth / 2);
                    break;
                }
                case 2: {
                    filterGray(0, 0, canvasDimensionWidth / 2, canvasDimensionHeigth);
                    break;
                }
                case 3: {
                    filterGray(0, 0, canvasDimensionWidth * 1 / 3, canvasDimensionHeigth);
                    break;
                }
                case 4: {
                    filterGray(0, 0, canvasDimensionWidth, canvasDimensionHeigth / 2);
                    break;
                }
            }

            break;
        }
        case 'gray2': {
            switch (templateid) {
                case 1: {
                    filterGray(0, canvasDimensionHeigth / 2, canvasDimensionWidth, canvasDimensionHeigth / 2);
                    break;
                }
                case 2: {
                    filterGray(canvasDimensionWidth / 2, 0, canvasDimensionWidth / 2, canvasDimensionHeigth);
                    break;
                }
                case 3: {
                    filterGray(canvasDimensionWidth * 1 / 3, 0, canvasDimensionWidth * 1 / 3, canvasDimensionHeigth);
                    break;
                }
                case 4: {
                    filterGray(0, canvasDimensionHeigth / 2, canvasDimensionWidth / 2, canvasDimensionHeigth / 2);
                    break;
                }
            }
            break;
        }
        case 'gray3': {
            switch (templateid) {
                case 1: {
                    filterGray(0, canvasDimensionHeigth / 2, canvasDimensionWidth, canvasDimensionHeigth / 2);
                    break;
                }
                case 2: {
                    filterGray(canvasDimensionWidth / 2, 0, canvasDimensionWidth / 2, canvasDimensionHeigth);
                    break;
                }
                case 3: {
                    filterGray(canvasDimensionWidth * 2 / 3, 0, canvasDimensionWidth * 1 / 3, canvasDimensionHeigth);
                    break;
                }
                case 4: {
                    filterGray(canvasDimensionWidth / 2, canvasDimensionHeigth / 2, canvasDimensionWidth / 2, canvasDimensionHeigth / 2);
                    break;
                }
            }
            break;
        }
        case 'removeEffects': {
            //in cazul in care se apasa pe remove events se redeseneaza canvasul fara ele
            drawTemplate(templateid);
            break;
        }
    }
}

function filterGray(x,y,cW,cH) {
    var canvas = $("#canvas")[0];
    var ctx = canvas.getContext("2d");
    //preiau datele dintr-o anumita parte a canvas-ului
    var imageData = ctx.getImageData(x,y, cW, cH);
    var pixels = imageData.data;
    //am modificat valorile pentru rgb astfel incar sa se formeze gri, cu niste valori apropiate de 0
    for (var i = 0; i < pixels.length; i += 4) {
        var r = pixels[i];
        var g = pixels[i + 1];
        var b = pixels[i + 2];
        var value = 0.3 * r + 0.3 * g + 0.3 * b;
        pixels[i] = value;
        pixels[i + 1] = value;
        pixels[i + 2] = value;
    }
    ctx.putImageData(imageData, x, y);
}

function filterBrightness(x, y,cW,cH) {
    var canvas = $("#canvas")[0];
    var ctx = canvas.getContext("2d");
      //preiau datele dintr-o anumita parte a canvas-ului
    var imageData = ctx.getImageData(x, y, cW, cH);
    var pixels = imageData.data;
    //am marit fiecare pixel cu valoarea 50 pentru luminozitate mai ridicata
    for (var i = 0; i < pixels.length; i += 4) {
        pixels[i] += 50;
        pixels[i + 1] += 50;
        pixels[i + 2] += 50;
    }
    ctx.putImageData(imageData, x, y);
}

function addCustomImage(event){
    var canvas = $("#canvas")[0];
    var ctx = canvas.getContext("2d");
    canvasDimensionWidth = canvas.width;
    canvasDimensionHeigth = canvas.height;
    //se pune o imagine din media pe tot canvasul
    var img = new Image();  
    img.src = 'media/image.jpg';
    ctx.drawImage(img, 0, 0, canvasDimensionWidth, canvasDimensionHeigth);
}